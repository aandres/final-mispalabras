# Final MisPalabras

Práctica final curso 2021-2022 (MisPalabras)

## Informacion de la práctica
* Nombre: Antonio Andres Perez
* Titulación: Ingeniería en sistemas de telecomunicaciones
* Login de GitLab/Labs: aandres
* Video básico (url): https://www.youtube.com/watch?v=5Kmt1Q_M4ZI
* Video de la parte opcional (url): https://www.youtube.com/watch?v=aVUEC6VrP3g
* Despliegue (url): http://aandres.pythonanywhere.com/

## Logins Admin Site
* admin/admin

## usuarios
* admin/admin
* admin2/pythonanywhere

## Parte obligatoria
* Comentarios
* Buscar palabra
* Mi pagina personal
* Home
* Votos
* XML y JSON

## Partes opcionales
* Favicon
* Comentarios XML y JSON

## Particularidades requeridas
* BeautifulSoup
* xmltodict
* El despliegue desde python anywhere no funciona correctamente
