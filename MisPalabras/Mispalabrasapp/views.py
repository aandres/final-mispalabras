import json
import random
import urllib.request

import xmltodict
from django.contrib.auth import logout, authenticate, login
from django.http import HttpResponse, Http404
from django.shortcuts import redirect, render
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User

from .models import Palabra, Comentario, Enlace, Voto, RAE
from bs4 import BeautifulSoup

def ordenar_palabras(request, palabras_list):
    sorted_words = list(range(10))
    sorted_words = sorted(palabras_list, key=lambda x: x.votos, reverse=True)

    return sorted_words

@csrf_exempt
def index(request):
    msg = ""
    msg1 = ""

    if request.method == "POST":
        action = request.POST["action"]

        if action == "Registrarse":
            username = request.POST["user"]
            password = request.POST["password"]

            if User.DoesNotExist:
                user = User.objects.create_user(username, password)
                user.save()
            else:
                msg = "El usario ya está registrado"

        elif action == "Iniciar Sesion":
            username = request.POST["user"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user:
                login(request, user)

        elif action == "Logout":
            logout(request)

        elif action == "Submit Word":
            palabra = request.POST["palabra"]
            return redirect("/" + palabra)

    try:
        user = User.objects.get(username=username)
        user_authenticated = True
    except:
        msg1 = ""

    count = 0
    palabras_list = list()
    sorted_words = list()
    try:
        palabras_list = Palabra.objects.all()
        sorted_words = ordenar_palabras(request, palabras_list)
        for palabra in palabras_list:
            count += 1
        if len(palabras_list) > 5:
            palabras_list = list(palabras_list)
            palabras_list = palabras_list[::-1]
            palabras_list = palabras_list[0:5]
            palabras_list = palabras_list[::-1]
        else:
            msg = ""

    except Palabra.DoesNotExist:
        msg = ""

    user_authenticated = request.user.is_authenticated
    random_p = Palabra()
    try:
        palabras_list = Palabra.objects.all()
        random_p = Palabra()
        random_p = random.choice(palabras_list)
    except:
        msg = ""

    context = {'user_authenticated': user_authenticated, 'msg': msg, 'palabras_list': palabras_list,
               'num_palabras': count, 'palabra': random_p, 'msg1': msg1, 'sorted_words': sorted_words}

    return render(request, "html/index.html", context)

def user_page(request):
    my_user = request.user

    msg = ""
    msg1 = ""
    msg2 = ""
    msg3 = ""
    palabras_list = list()
    enlace_list = list()
    comentarios_list = list()

    try:
        palabras_list = Palabra.objects.filter(usuario=my_user)
        palabras_list = palabras_list.all()
        palabras_list = palabras_list[::-1]
        palabras_list = palabras_list[0:3]
        palabras_list = palabras_list[::-1]
    except Palabra.DoesNotExist:
        msg1 = "Todavía no has almacenado ninguna palabra"

    try:
        enlace_list = Enlace.objects.filter(usuario=my_user)
        enlace_list = enlace_list.all()
        enlace_list = enlace_list[::-1]
        enlace_list = enlace_list[0:3]
        enlace_list = enlace_list[::-1]
    except Enlace.DoesNotExist:
        msg2 = "Todavía no has subido ningún enlace"

    try:
        comentarios_list = Comentario.objects.filter(usuario=my_user)
        comentarios_list = comentarios_list.all()
        comentarios_list = comentarios_list[::-1]
        comentarios_list = comentarios_list[0:3]
        comentarios_list = comentarios_list[::-1]
    except Comentario.DoesNotExist:
        msg3 = "Todavía no has subido ningún comentario"

    count = 0
    try:
        palabras_list = Palabra.objects.all()
        for p in palabras_list:
            count += 1
    except Palabra.DoesNotExist:
        msg = ""

    random_p = Palabra()
    try:
        palabras_list = Palabra.objects.all()
        random_p = Palabra()
        random_p = random.choice(palabras_list)
    except:
        msg = ""

    context = {'palabras_list': palabras_list, 'enlace_list': enlace_list, 'comentarios_list': comentarios_list,
               'msg1': msg1, 'msg2': msg2, 'msg3': msg3, 'random_p': random_p, 'num_palabras': count,
               'msg': msg}

    return render(request, "html/user.html", context)

def palabra_page(request, palabra):
    msg = ""
    p_aux = Palabra()

    url_xml = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles="+ palabra +"&prop=extracts&exintro&explaintext"
    xml = urllib.request.urlopen(url_xml)
    xml_dict = xmltodict.parse(xml.read())
    try:
        descripcion = xml_dict["api"]["query"]["pages"]["page"]["extract"]['#text']
    except:
        raise Http404("Esta palabra no existe en Wikipedia en español")

    url_json = "https://es.wikipedia.org/w/api.php?action=query&titles="+palabra+"&prop=pageimages&format=json&pithumbsize=200"
    json_file = urllib.request.urlopen(url_json)
    json_dict = json.loads(json_file.read().decode('utf8'))
    id_json = str(xml_dict["api"]["query"]["pages"]["page"]["@pageid"])
    imagen = json_dict["query"]["pages"][id_json]["thumbnail"]["source"]

    url = "https://dle.rae.es/" + palabra
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(req) as response:
        html = response.read().decode('utf8')
    soup = BeautifulSoup(html, 'html.parser')
    content = soup.find('meta', property="og:description")
    descripcion_rae = content['content']

    fecha = timezone.now
    r = RAE(fecha=fecha, palabra=palabra, usuario=request.user, descripcion=descripcion_rae)
    r.save()


    url_xml = "https://www.flickr.com/services/feeds/photos_public.gne?tags=" + palabra
    xml = urllib.request.urlopen(url_xml)
    xml_dict = xmltodict.parse(xml.read())
    imagen_flickr = xml_dict["feed"]["entry"][0]["link"][1]["@href"]

    if request.method == "POST":
        action = request.POST["action"]
        if action == "Registrarse":
            username = request.POST["user"]
            password = request.POST["password"]

            if User.DoesNotExist:
                if username and password:
                    user = User.objects.create_user(username, password)
                    user.save()
            else:
                msg = "El usuario ya existe"

        elif action == "Iniciar Sesion":
            username = request.POST["user"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
            else:
                msg = "Usuario o contraseña incorrectas"
        elif action == "Logout":
            logout(request)
            return redirect("/" + palabra)
        elif action == "Almacenar":
            nombre = palabra
            fecha = timezone.now
            user = request.user
            p = Palabra(titulo=nombre, fecha=fecha, usuario=user, descripcion=descripcion, imagen=imagen)
            p.save()
        elif action == "Submit Word":
            palabra = request.POST["palabra"]
            return redirect("/" + palabra)
        elif action == "comentar":
            comentario = request.POST["comentario"]
            fecha = timezone.now
            user = request.user
            p_aux = Palabra.objects.get(titulo=palabra)
            c = Comentario(contenido=comentario, fecha=fecha, palabra=p_aux, usuario=user)
            c.save()
        elif action == "enlazar":
            enlace = request.POST["enlace"]
            req = urllib.request.urlopen(enlace)
            soup = BeautifulSoup(req, 'html.parser')
            title_enl = soup.find('meta', property='og:title')
            if title_enl:
                title = title_enl['content']
            else:
                title = "No hay título para este enlace"
            image_enl = soup.find('meta', property='og:image')
            if image_enl:
                imagen = image_enl['content']
            else:
                imagen = "No hay imagen para este enlace"
            contenido_enl = soup.find('meta', property='og:description')
            if contenido_enl:
                contenido = contenido_enl['content']
            else:
                contenido = "No hay contenido para este enlace"

            fecha = timezone.now
            user = request.user
            p_aux = Palabra.objects.get(titulo=palabra)
            e = Enlace(enlace=enlace, contenido=contenido, titulo=title,
                       imagen=imagen, fecha=fecha, palabra=p_aux, usuario=user)
            e.save()
        elif action == "votar":
            p = Palabra.objects.get(titulo=palabra)
            p.votos = p.votos + 1
            p.save()
            user = request.user
            is_voted = True
            p_aux = Palabra.objects.get(titulo=palabra)
            v = Voto(voted=is_voted, palabra=p_aux, usuario=user)
            v.save()

    does_exist = False
    try:
        p = Palabra.objects.get(titulo=palabra)
        does_exist = True
    except Palabra.DoesNotExist:
        does_exist = False

    comentario_list = list()
    vote = Voto()
    enlace_list = list()
    palabras_list = list()

    if does_exist:
        p_id = Palabra.objects.get(titulo=palabra).id
        comentarios = Comentario.objects.all()
        for comentario in comentarios:
            if comentario.palabra_id == p_id:
                comentario_list.append(comentario)

        enlaces = Enlace.objects.all()
        for enlace in enlaces:
            if enlace.palabra_id == p_id:
                enlace_list.append(enlace)

        try:
            p_id = Palabra.objects.get(titulo=palabra).id
            vote = Voto.objects.get(palabra=p_id, usuario=request.user)
        except Voto.DoesNotExist:
            msg = ""

    user_authenticated = request.user.is_authenticated

    count = 0
    sorted_words = list()
    try:
        palabras_list = Palabra.objects.all()
        sorted_words = ordenar_palabras(request, palabras_list)
        for p in palabras_list:
            count += 1
        if len(palabras_list) > 5:
            palabras_list = list(palabras_list)
            palabras_list = palabras_list[::-1]
            palabras_list = palabras_list[0:5]
            palabras_list = palabras_list[::-1]
        else:
            msg = ""

    except Palabra.DoesNotExist:
        msg = "No hay palabras almacenadas"
    random_p = Palabra()
    try:
        palabras_list = Palabra.objects.all()
        random_p = Palabra()
        random_p = random.choice(palabras_list)
    except:
        msg = ""

    if does_exist:
        context = {'palabra': palabra, 'descripcion': descripcion, 'imagen': imagen, 'does_exist': does_exist,
               'user_authenticated': user_authenticated, 'def_rae': descripcion_rae,
               'imagen_flickr': imagen_flickr, 'comentario_list': comentario_list, 'enlace_list': enlace_list,
               'vote': vote, 'p_object': p_aux, 'msg': msg, 'palabras_list': palabras_list,
               'num_palabras': count, 'random_p': random_p, 'sorted_words': sorted_words}
    else:
        context = {'palabra': palabra, 'descripcion': descripcion, 'imagen': imagen, 'does_exist': does_exist,
                   'user_authenticated': user_authenticated, 'def_rae': descripcion_rae,
                   'imagen_flickr': imagen_flickr, 'comentario_list': comentario_list, 'enlace_list': enlace_list,
                   'vote': vote, 'msg': msg, 'palabras_list': palabras_list, 'num_palabras': count,
                   'random_p': random_p, 'sorted_words': sorted_words}

    return render(request, "html/palabra.html", context)

def ayuda_page(request):
    msg = ""
    count = 0

    try:
        palabras_list = Palabra.objects.all()
        for p in palabras_list:
            count += 1
    except Palabra.DoesNotExist:
        msg = ""

    try:
        palabras_list = Palabra.objects.all()
        random_p = Palabra()
        random_p = random.choice(palabras_list)
    except:
        msg = ""


    context = {'random_p': random_p, 'num_palabras': count, 'msg': msg}

    return render(request, "html/ayuda.html", context)

def palabras_xml(request):
    msg = ""
    palabras_list = list()
    try:
        palabras_list = Palabra.objects.all()
    except Palabra.DoesNotExist:
        msg = "Todavía no hay palabras almacenadas"

    content = {'palabras_list': palabras_list}

    return render(request, "xml/palabras.xml", content, content_type="text/xml")

def palabras_json(request):
    msg = ""
    palabras_list = list()
    try:
        palabras_list = Palabra.objects.all()
    except Palabra.DoesNotExist:
        msg = "Todavía no hay palabras almacenadas"

    content = {'palabras_list': palabras_list}

    return render(request, "json/palabras.json", content)

def palabra_xml(request, palabra):
    msg = ""
    p = Palabra()
    try:
        p = Palabra.objects.get(titulo=palabra)
    except Palabra.DoesNotExist:
        msg = "Todavía no hay palabras almacenadas"

    content = {'p': p}

    return render(request, "xml/palabra.xml", content, content_type="text/xml")

def palabra_json(request, palabra):
    msg = ""
    p = Palabra()
    try:
        p = Palabra.objects.get(titulo=palabra)
    except Palabra.DoesNotExist:
        msg = "Todavía no hay palabras almacenadas"

    content = {'p': p}

    return render(request, "json/palabra.json", content, content_type="text/json")

def comentarios_xml(request, palabra):
    msg = ""
    comentarios = Comentario()
    comentario_list = list()
    try:
        p_id = Palabra.objects.get(titulo=palabra).id
        comentarios = Comentario.objects.all()
        for comentario in comentarios:
            if comentario.palabra_id == p_id:
                comentario_list.append(comentario)
    except Palabra.DoesNotExist:
        msg = "No hay ningún comentario para esta palabra"

    content = {'comentario_list': comentario_list, 'msg': msg}

    return render(request, "xml/comentarios.xml", content, content_type="text/xml")

def comentarios_json(request, palabra):
    msg = ""
    comentarios = Comentario()
    comentario_list = list()
    try:
        p_id = Palabra.objects.get(titulo=palabra).id
        comentarios = Comentario.objects.all()
        for comentario in comentarios:
            if comentario.palabra_id == p_id:
                comentario_list.append(comentario)
    except Palabra.DoesNotExist:
        msg = "No hay ningún comentario para esta palabra"

    content = {'comentarios_list': comentario_list, 'msg': msg}

    return render(request, "json/comentarios.json", content, content_type="text/json")
