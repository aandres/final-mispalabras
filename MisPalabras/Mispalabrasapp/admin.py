from django.contrib import admin
from .models import Palabra, Comentario

admin.site.register(Palabra)
admin.site.register(Comentario)